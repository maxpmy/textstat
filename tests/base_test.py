import os


class BaseTest:

    @classmethod
    def assert_paths_exist(cls, json_response, paths):
        """
        Asserts tha all paths in :paths exist in :json_response
        """
        for path in paths:
            steps = path.split('.')
            if len(steps) == 1:
                assert steps[0] in json_response
            else:
                cls.assert_paths_exist(json_response[steps[0]],
                                       ['.'.join(steps[1:])])

    @staticmethod
    def read_file(directory, filename):
        """ Reads file from directory """
        with open(os.path.join(directory, filename), 'r') as f:
            data = f.read()
        return data
