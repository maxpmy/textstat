"""
Tests
"""
import pytest
import os

from conftest import (
    VALID_TEST_FILES,
    TEST_DATA_DIR,
    TEST_CASES,
)
from base_test import BaseTest


class CommonGetTests(BaseTest):
    """
    Basic GET endpoint tests
    Is executed by all child classes
    """
    pattern = None
    required_keys = None

    @pytest.mark.parametrize('filename', VALID_TEST_FILES)
    def test_valid(self, client, index_test_cases, filename):
        """
        Test that endpoint returns 200 for valid files and response contains all required keys
        """
        text_id = index_test_cases[filename]
        r = client.get(self.pattern.format(text_id=text_id))
        assert r.status_code == 200
        self.assert_paths_exist(r.json, self.required_keys)

    def test_invalid(self, client, index_test_cases):
        """
        Test that endpoint returns 500 for unindexed document
        """
        r = client.get(self.pattern.format(text_id='fakeid'))
        assert r.status_code == 500


class TestGetFile(CommonGetTests):
    """
    Business logic tests for -GET /{text_id} endpoint
    """
    pattern = '/{text_id}'
    required_keys = ['text_id', 'raw_text']

    @pytest.mark.parametrize('filename', VALID_TEST_FILES)
    def test_get_file_text(self, client, index_test_cases, filename):
        """
        Test /{text_id} returns correct raw text
        """
        text_id = index_test_cases[filename]
        text = self.read_file(TEST_DATA_DIR, filename)
        r = client.get('/{}'.format(text_id))
        assert r.json['raw_text'] == text


class TestGetStats(CommonGetTests):
    """
    Business logic tests for -GET /stats/{text_id} endpoint
    """
    pattern = '/stats/{text_id}'
    required_keys = ['text_id', 'stats', 'stats.terms', 'stats.word_count']

    @pytest.mark.parametrize('filename,expected_stats,word_count,stopwords', TEST_CASES)
    def test_get_file_stats(self, client, index_test_cases, filename, expected_stats, word_count, stopwords):
        """
        Test /stats/{text_id} returns stats for
        Tests that all test text files' stats are as expected
        """
        # Retrieve file stats by filename
        text_id = index_test_cases[filename]
        url = '/stats/{}'.format(text_id)
        if stopwords: # some test cases test for stopwords
            url += '?stopwords={}'.format(','.join(stopwords))

        stats = client.get(url).json
        terms = stats['stats']['terms']

        # Check that the word count is correct
        assert stats['stats']['word_count'] == word_count

        # Check that output terms set is correct
        assert set(terms.keys()) == set(expected_stats.keys())

        # Check that output term frequency is correct
        for term, count in terms.items():
            assert count == expected_stats[term]
