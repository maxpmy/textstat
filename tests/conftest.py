import pytest
import os

from textstat.app.app import create_app


TEST_DATA_DIR = 'tests/data'
TEST_CASES = [
    ('one.txt', {
        'foo': 2,
        'bar': 2,
        'foobar': 2,
        'foofighters': 1,
        }, 7, None),
    ('two.txt', {
        'a': 2,
        'blue': 2,
        'blueberry': 1,
        'went': 1,
        'to': 1,
        'bluegrass': 1,
        'concert': 1,
        'and': 1,
        'felt': 1,
        'afterwards': 1,
    }, 12, None),
    ('two.txt', {
        'a': 2,
        'to': 1,
        'concert': 1,
        'and': 1,
        'felt': 1,
        'afterwards': 1,
    }, 7, ['blue', 'went']),
]
VALID_TEST_FILES = set(zip(*TEST_CASES)[0])


@pytest.fixture(scope='session')
def app(request):
    """
    Creates app
    Envoked once per test suite session
    """
    app = create_app()
    yield app
    # Teardown test index after test suite exits
    teardown(app)


@pytest.fixture(scope='function')
def index_test_cases(app, client):
    """
    Indexes all test text files to elasticsearch
    Removes the index as test setup and teardown
    """

    index_name = app.config.index_name
    doc_type = app.config.doc_type

    # Remove previous test index in case test died before teardown
    app.es.indices.delete(index=index_name, ignore=[400, 404])

    # Index test files
    name_to_id = {}
    for filename in VALID_TEST_FILES:
        with open(os.path.join(TEST_DATA_DIR, filename), 'r') as f:
            es_doc = {'text': f.read()}
            res = app.es.index(
                index=index_name,
                doc_type=doc_type,
                body=es_doc
            )
            name_to_id[filename] = res['_id']

    yield name_to_id
    # Teardown test index after test exits
    teardown(app)


def teardown(app):
    """
    Removes test index
    """
    app.es.indices.delete(index=app.config.index_name, ignore=404)
