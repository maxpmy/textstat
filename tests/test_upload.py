import pytest

from conftest import (
    TEST_DATA_DIR,
    VALID_TEST_FILES,
)
from base_test import BaseTest


class TestUpload(BaseTest):

    @pytest.mark.parametrize('filename', VALID_TEST_FILES)
    def test_file_upload_valid(self, client, filename):
        """
        Test that file upload endpoint successfully sends text data to elasticsearch
        """
        # Read text file from test data directory
        text = self.read_file(TEST_DATA_DIR, filename)

        # Post the text using /upload endpoint
        r = client.post('/upload',
                        content_type='text/plain',
                        data=text)

        assert r.status_code == 200

        # Query elasticsearch to verify that data was posted correctly
        app = client.application
        doc = app.es.get(index=app.config.index_name,
                        doc_type=app.config.doc_type,
                        id=r.json['text_id'])
        assert doc, 'No document found for {}'.format(filename)
        assert doc['_source']['text'] == text, 'Wrong text for {}'.format(filename)


    @pytest.mark.parametrize('text_size,expected_status_code', [
        (10 * 10**6, 200),
        (10 * 10**6 + 1, 500),
    ])
    def test_text_upload_size(self, client, text_size, expected_status_code):
        """
        Test that can only upload text <= 10**7 bytes
        """
        text = '0' * text_size

        # Post the text using /upload endpoint
        r = client.post('/upload',
                        content_type='text/plain',
                        data=text)

        assert r.status_code == expected_status_code

    def test_file_upload_non_ascii(self, client):
        """
        Test that can not upload texts with non-ascii characters
        """
        text = 'Les Mis\xc3rables'

        r = client.post('/upload',
                        content_type='text/plain',
                        data=text)

        assert r.status_code == 500
