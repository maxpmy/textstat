# App
export APP_HOST='0.0.0.0'
export APP_PORT=80
export APP_DEBUG_MODE=true
export APP_DOC_TYPE='texts'
export APP_INDEX_NAME='textstat'

# Elasticsearch
export ES_HOST = 'localhost'
export ES_PORT=9200
export ES_USER=None
export ES_PASS=None

python app/app.py
