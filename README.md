# Pre-requisites:
- Python `v2.7.10`

# Installation

1. Install required python libraries

    ➜  `pip install -r requirements.txt`

2. Download and unpack elasticsearch 

    ➜  `curl https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.4.2.tar.gz`

    ➜  `tar -xvzf elasticsearch-6.4.2.tar.gz`

# Running
* App

    ➜  `elasticsearch-6.4.2/bin/elasticsearch` (required to run)

    ➜  `sh ./run_app.sh`

* Tests

    ➜  `elasticsearch-6.4.2/bin/elasticsearch` (required to run)

    ➜  `sh ./run_tests.sh`

# Usage (assuming the app is running)
##  Submitting text from string

➜  `curl -POST -H "Content-Type: text/plain" -d "To be or not to be?" "localhost/upload"`

```
    {
      "stats": {
        "terms": {
          "be": 2,
          "not": 1,
          "or": 1,
          "to": 2
        },
        "word_count": 6
      },
      "text_id": "lB1xn2YBPfHN45OccpQG"
    }
```

##  Submitting text from a file

`tests/data/one.txt`:
```
    Foo bar, foobar.Foo!Bar!#,@$!Foobar,foofighters
```

➜  `curl -POST -H "Content-Type: text/plain" -d @tests/data/one.txt "localhost/upload"`

```
    {
      "stats": {
        "terms": {
          "bar": 2,
          "foo": 1,
          "foobar": 1,
          "foobar.foo": 1,
          "foofighters": 1
        },
        "word_count": 6
      },
      "text_id": "kx1tn2YBPfHN45OcgZTO"
    }
```

##  Getting submitted text

➜  `curl -GET "localhost/kx1tn2YBPfHN45OcgZTO"`

```
    {
      "raw_text": "Foo bar, foobar.Foo!Bar!#,@$!Foobar,foofighters",
      "text_id": "kx1tn2YBPfHN45OcgZTO"
    }
```

##  Getting previously submitted text stats

➜  `curl -GET localhost/stats/kx1tn2YBPfHN45OcgZTO`

```
    {
      "stats": {
        "terms": {
          "bar": 2,
          "foo": 1,
          "foobar": 1,
          "foobar.foo": 1,
          "foofighters": 1
        },
        "word_count": 6
      },
      "text_id": "kx1tn2YBPfHN45OcgZTO"
    }
```

##  Stop words filtering
### Querying old text

`text_id="lB1xn2YBPfHN45OccpQG"`
```
To be or not to be?
```

➜  `curl -GET "localhost/stats/lB1xn2YBPfHN45OccpQG?stopwords=not,to"`

```
    {
      "stats": {
        "terms": {
          "be": 2,
          "or": 1
        },
        "word_count": 3
      },
      "stopwords": [
        "not",
        "to"
      ],
      "text_id": "lB1xn2YBPfHN45OccpQG"
    }
```

### During upload

`tests/data/two.txt`:
```
    A blue blueberry went to a bluegrass concert and felt blue afterwards.
```

➜  `curl -POST -H "Content-Type: text/plain" -d @tests/data/two.txt "localhost/upload?stopwords=blue"`

```
    {
      "stats": {
        "terms": {
          "a": 2,
          "afterwards": 1,
          "and": 1,
          "concert": 1,
          "felt": 1,
          "to": 1,
          "went": 1
        },
        "word_count": 8
      },
      "stopwords": [
        "blue"
      ],
      "text_id": "lh2Rn2YBPfHN45OcKJSq"
    }
```
