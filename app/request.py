
def parse_args(flask_request):
    """
    Parse url arguments
    """
    parsed = {}
    stopwords = flask_request.args.get('stopwords')
    if stopwords:
        stopwords = stopwords.split(',')
        parsed['stopwords'] = stopwords
    return parsed


def validate_content_type(decorated_function):
    """
    Decorator to validate that Content-Type == 'text/plain'
    """
    def wrapped(flask_request):
        content_type = flask_request.content_type
        if content_type != 'text/plain':
            raise Exception('Content-Type "{}" is not supported'
                            .format(content_type))
        return decorated_function(flask_request)

    return wrapped


def validate_content_length(decorated_function):
    """
    Decorator to validate upload size is less then 10MB
    """
    def wrapped(flask_request):
        mb = 10.**6  # Using SI definition of MegaByte
        max_data_size = 10 * mb
        text_size = flask_request.content_length
        if text_size > max_data_size:
            raise Exception(
                "Can not upload files bigger then {}MB. Attempted size: {}MB"
                .format(max_data_size/mb, text_size/mb)
            )
        return decorated_function(flask_request)

    return wrapped


def validate_encoding(decorated_function):
    """
    Decorator to validate that post data not ASCII-encoded
    """
    def wrapped(flask_request):
        data = flask_request.data
        try:
            data.decode('ascii')
        except UnicodeDecodeError:
            raise Exception('Data should be ASCII-encoded')
        return decorated_function(flask_request)

    return wrapped


def validate_non_empty(decorated_function):
    """
    Decorator to validate that post data is not empty
    """
    def wrapped(flask_request):
        text_data = flask_request.data
        if not text_data:
            raise Exception("Can not submit empty text")
        return decorated_function(flask_request)
    
    return wrapped