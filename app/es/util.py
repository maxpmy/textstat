
def assert_text_exists(decorated_function):
    """
    Decorator to assert that document with :text_id
    is present in :app's elasticsearch
    """
    def wrapped(app, text_id, **kwargs):
        es_args = {
            'index': app.config.index_name,
            'doc_type': app.config.doc_type,
            'id': text_id,
        }
        if not app.es.exists(**es_args):
            raise Exception('Text with text_id={} does not exist'
                            .format(text_id))
        return decorated_function(app, text_id, **kwargs)

    return wrapped