es_mapping = {
    "texts": {
        "properties": {
            "text": {
                "type": "text",
                "analyzer": "my_analyzer"
            }
        }
    }
}

es_settings = {
    "analysis": {
        "analyzer": {
            "my_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": ["lowercase"]
            }
        }
   }
}