import sys


from es.util import assert_text_exists
from request import (
    validate_content_type,
    validate_content_length,
    validate_non_empty,
    validate_encoding,
)


@validate_encoding
@validate_non_empty
@validate_content_type
@validate_content_length
def prepare_upload_document(flask_request):
    """
    Retrieves upload data, checks if valid, returns the document to index
    """
    # Get uploaded text data
    text_data = flask_request.data

    # Return document to index
    es_document = {'text': text_data}
    return es_document


@assert_text_exists
def get_text_stats(app, text_id, stopwords=None):
    """
    Returns text stats
    :app - Flask app object
    :text_id - elasticsearch document id for uploaded text
    :stopwords - coma-separated words to remove from output
    """

    query = {
        "fields": ["text"],
        "term_statistics": True,
        "offsets": False,
        "payloads": False,
        "positions": False
    }
    terms = app.es.termvectors(
        index=app.config.index_name,
        doc_type=app.config.doc_type,
        id=text_id,
        body=query
    )['term_vectors']['text']['terms']

    if stopwords:
        # filter out stopword terms
        terms = {k: v for k, v in terms.items()
                 if all([sw not in k for sw in stopwords])}

    terms = {k: v['term_freq'] for k, v in terms.items()}
    word_count = sum([terms[t] for t in terms])

    return {
        'terms': terms,
        'word_count': word_count,
    }
