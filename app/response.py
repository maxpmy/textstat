
def standard_response(text_id, **optional_args):
    """
    Assembles response for all endpoints
    """
    response = {
        'text_id': text_id,
    }

    # Populate optional response args if value not empty
    for arg, value in optional_args.items():
        if value:
            response[arg] = value

    return response
