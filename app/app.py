import os
from flask import Flask
from elasticsearch import Elasticsearch

from endpoints import blueprint
from es.mapping import (
    es_mapping,
    es_settings,
)


def create_app():
    app = Flask(__name__)
    app.register_blueprint(blueprint)
    init_db(app)
    return app


def init_db(app):
    # Connect to Elasticsearch:
    host = os.environ.get('ES_HOST', 'localhost')
    port = os.environ.get('ES_PORT', 9200)
    user = os.environ.get('ES_USER')
    password = os.environ.get('ES_PASS')
    app.config.index_name = os.environ.get('APP_INDEX_NAME')
    app.config.doc_type = os.environ.get('APP_DOC_TYPE')
    app.es = Elasticsearch(
        [host],
        http_auth=(user, password),
        port=port
    )

    # Create index if needed
    app.es.indices.create(
        app.config.index_name, ignore=400,
        body={"settings": es_settings,
              "mappings": es_mapping}
    )


if __name__ == "__main__":
    host = os.environ.get('APP_HOST', '0.0.0.0')
    port = os.environ.get('APP_PORT', 80)
    debug = os.environ.get('APP_DEBUG_MODE', False)
    app = create_app()
    app.run(host=host, port=port, debug=debug)
