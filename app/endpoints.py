from flask import (
    Blueprint,
    jsonify,
    current_app as app,
    request,
    abort,
)

from core import (
    prepare_upload_document,
    get_text_stats,
)
from request import parse_args
from response import standard_response

blueprint = Blueprint('profile', __name__)


@blueprint.route('/upload', methods=['POST'])
def upload_text_file():
    """
    Handles text file upload.
    Indexes submitted text as elasticsearch document and returns stats right away
    """

    # Parse POST data
    es_doc = prepare_upload_document(request)

    # Index document into elasticsearch
    res = app.es.index(
        index=app.config.index_name,
        doc_type=app.config.doc_type,
        body=es_doc
    )

    # Get and return stats
    text_id = res.get('_id')
    stopwords = parse_args(request).get('stopwords')
    stats = get_text_stats(app, text_id, stopwords=stopwords)

    return jsonify(
        standard_response(text_id, stats=stats, stopwords=stopwords)
    )


@blueprint.route('/<string:text_id>', methods=['GET'])
def get_text(text_id):
    """
    Retrieve text as stored in elasticsearch by :text_id
    """
    es_args = {
        'index': app.config.index_name,
        'doc_type': app.config.doc_type,
        'id': text_id,
    }
    es_doc = app.es.get(**es_args)
    return jsonify(
        standard_response(text_id, raw_text=es_doc['_source']['text'])
    )


@blueprint.route('/stats/<string:text_id>', methods=['GET'])
def get_stats(text_id):
    """
    Return text stats for :text_id
    Accepts optional url parameter "stopwords" to remove certain words from input

    Example usage:
        To lookup text stats for previously uploaded text file with text_id="text_id"
        and skip word containing "one", "two" and "three":
            curl -GET root_url/stats/mytext_id?stopwords=one,two,three

    """
    stopwords = parse_args(request).get('stopwords')

    stats = get_text_stats(app, text_id, stopwords=stopwords)
    return jsonify(
        standard_response(text_id, stats=stats, stopwords=stopwords)
    )
